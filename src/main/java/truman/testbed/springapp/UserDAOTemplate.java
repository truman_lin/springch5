package truman.testbed.springapp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

public class UserDAOTemplate implements IUserDAO {
	@SuppressWarnings("deprecation")
	private SimpleJdbcTemplate template;
	
	@SuppressWarnings("deprecation")
	public void setDataSource(DataSource ds) {
		template=new SimpleJdbcTemplate(ds);
	}

	@SuppressWarnings("deprecation")
	public void insert(User user) {
		String sql="INSERT INTO user(name,age) VALUES(?,?)";
		String name = user.getName();
		int age = user.getAge();
		
		template.update(sql, new Object[] {name,age});
	}

	@SuppressWarnings("deprecation")
	public User find(Integer id) {
		String sql="SELECT * FROM user WHERE ID=?";
		ParameterizedRowMapper<User> mapper= new ParameterizedRowMapper<User>(){
			public User mapRow(ResultSet rs, int rowNum) throws SQLException{
				User user = new User();
				user.setAge(rs.getInt("age"));
				user.setId(rs.getInt("id"));
				user.setName(rs.getString("name"));
				return user;

			}
		};
		return template.queryForObject(sql, mapper, id);
	}
}
