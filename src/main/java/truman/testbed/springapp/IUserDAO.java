package truman.testbed.springapp;

public interface IUserDAO {
	void insert(User user);
	User find(Integer id);
}
