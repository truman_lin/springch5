package truman.testbed.springapp;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringDAODemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"dao-config.xml");
		User user = new User();
		user.setName("Jophy");
		user.setAge(9);
		IUserDAO userDAO=(IUserDAO) context.getBean("userDAO");
		//userDAO.insert(user);
		user = userDAO.find(3);
		System.out.println("user name: "+user.getName());
	}

}
